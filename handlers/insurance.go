package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type Insurance struct {
	Plans []struct {
		ResponseType int    `json:"response_type"`
		ID           string `json:"id"`
		Name         string `json:"name"`
	} `json:"plans"`
}

func InsuranceList(c *gin.Context) {
	insurance, err := ioutil.ReadFile("insurance.txt")
	if err != nil {
		log.Fatal(err)
	}

	var insurance_list []string

	insuranceString := string(insurance)
	insuranceArray := strings.Split(insuranceString, "],[")

	for i := 0; i < len(insuranceArray); i++ {
		dataString := string(insuranceArray[i])
		data := strings.Split(dataString, ",")

		insurance_list = append(insurance_list, data[1])
	}

	c.JSON(200, gin.H{"list": insurance_list})
}

func InsurancePlans(c *gin.Context) {
	insurance, err := ioutil.ReadFile("insurance.txt")
	if err != nil {
		log.Fatal(err)
	}

	var insurance_url []string

	initial_url := "https://www.zocdoc.com/api/1/carriers/"
	end_url := "/plans?type=2&tier=0"

	insuranceString := string(insurance)
	insuranceArray := strings.Split(insuranceString, "],[")

	for i := 0; i < len(insuranceArray); i++ {
		dataString := string(insuranceArray[i])
		data := strings.Split(dataString, ",")
		url := initial_url + data[0] + end_url

		insurance_url = append(insurance_url, url)
	}

	for i := 0; i < len(insurance_url); i++ {
		//var insurance = &Insurance{}
		res, err := http.Get(insurance_url[i])
		if err != nil {
			fmt.Println("Something went wrong")
		}

		defer res.Body.Close()

		result, err := ioutil.ReadAll(res.Body)
		if err != nil {
			panic(err)
		}

		stringResult := string(result)
		resultFormatted := strings.Replace(stringResult, "for(;;);", "", 1)

		fmt.Println(json.Unmarshal(byte(resultFormatted), &insurance))

	}

	//	fmt.Println(inusrance_url)

	c.JSON(200, gin.H{"urls": insurance_url})
	// Sample http request for getting plans of insurance company
}

func DeleteItem(c *gin.Context) {
	creds := credentials.NewSharedCredentials("/Users/gentlyawesome/.awscredentials", "default")

	credsValue, err := creds.Get()

	if err != nil {
		panic(err)
	}

	fmt.Println(credsValue)

	sess, err := session.NewSession(&aws.Config{
		CredentialsChainVerboseErrors: aws.Bool(true),
		Region:      aws.String("us-east-1"),
		Endpoint:    aws.String("http://localhost:8000/"),
		Credentials: creds,
	})
	if err != nil {
		fmt.Println("failed to create session", err)
		return
	}

	svc := dynamodb.New(sess)

	params := &dynamodb.DeleteItemInput{
		TableName: aws.String("Insurance"),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String("test"),
			},
		},
	}

	res, err := svc.DeleteItem(params)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(res)
}

func GetItem(c *gin.Context) {
	creds := credentials.NewSharedCredentials("/Users/gentlyawesome/.awscredentials", "default")

	credsValue, err := creds.Get()

	if err != nil {
		panic(err)
	}

	fmt.Println(credsValue)

	sess, err := session.NewSession(&aws.Config{
		CredentialsChainVerboseErrors: aws.Bool(true),
		Region:      aws.String("us-east-1"),
		Endpoint:    aws.String("http://localhost:8000/"),
		Credentials: creds,
	})
	if err != nil {
		fmt.Println("failed to create session", err)
		return
	}

	svc := dynamodb.New(sess)

	params := &dynamodb.GetItemInput{
		TableName: aws.String("Insurance"),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String("test"),
			},
		},
	}

	res, err := svc.GetItem(params)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(res)
}

func UpdateItem(c *gin.Context) {
	creds := credentials.NewSharedCredentials("/Users/gentlyawesome/.awscredentials", "default")

	credsValue, err := creds.Get()

	if err != nil {
		panic(err)
	}

	fmt.Println(credsValue)

	sess, err := session.NewSession(&aws.Config{
		CredentialsChainVerboseErrors: aws.Bool(true),
		Region:      aws.String("us-east-1"),
		Endpoint:    aws.String("http://localhost:8000/"),
		Credentials: creds,
	})
	if err != nil {
		fmt.Println("failed to create session", err)
		return
	}

	svc := dynamodb.New(sess)

	params := &dynamodb.UpdateItemInput{
		TableName: aws.String("Insurance"),
		Key: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String("test"),
			},
		},
		AttributeUpdates: map[string]*dynamodb.AttributeValueUpdate{
			"Name": {
				Value: &dynamodb.AttributeValue{
					S: aws.String("test1234"),
				},
			},
		},
	}

	res, err := svc.UpdateItem(params)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(res)
}

func InsertItem(c *gin.Context) {
	creds := credentials.NewSharedCredentials("/Users/gentlyawesome/.awscredentials", "default")

	credsValue, err := creds.Get()

	if err != nil {
		panic(err)
	}

	fmt.Println(credsValue)

	sess, err := session.NewSession(&aws.Config{
		CredentialsChainVerboseErrors: aws.Bool(true),
		Region:      aws.String("us-east-1"),
		Endpoint:    aws.String("http://localhost:8000/"),
		Credentials: creds,
	})
	if err != nil {
		fmt.Println("failed to create session", err)
		return
	}

	svc := dynamodb.New(sess)

	params := &dynamodb.PutItemInput{
		TableName: aws.String("Insurance"),
		Item: map[string]*dynamodb.AttributeValue{
			"Id": {
				S: aws.String("test123"),
			},
			"Name": {
				S: aws.String("insurance2"),
			},
		},
	}

	res, err := svc.PutItem(params)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println(res)
}

func CreateTable(c *gin.Context) {
	creds := credentials.NewSharedCredentials("/Users/gentlyawesome/.awscredentials", "default")

	credsValue, err := creds.Get()

	if err != nil {
		panic(err)
	}

	fmt.Println(credsValue)

	sess, err := session.NewSession(&aws.Config{
		CredentialsChainVerboseErrors: aws.Bool(true),
		Region:      aws.String("us-east-1"),
		Endpoint:    aws.String("http://localhost:8000/"),
		Credentials: creds,
	})
	if err != nil {
		fmt.Println("failed to create session", err)
		return
	}

	svc := dynamodb.New(sess)

	params := &dynamodb.CreateTableInput{
		TableName: aws.String("Insurance"),
		KeySchema: []*dynamodb.KeySchemaElement{
			{
				AttributeName: aws.String("Id"),
				KeyType:       aws.String("HASH"),
			},
		},
		AttributeDefinitions: []*dynamodb.AttributeDefinition{
			{
				AttributeName: aws.String("Id"),
				AttributeType: aws.String("S"),
			},
		},
		ProvisionedThroughput: &dynamodb.ProvisionedThroughput{ // Required
			ReadCapacityUnits:  aws.Int64(1), // Required
			WriteCapacityUnits: aws.Int64(1), // Required
		},
	}

	res, err := svc.CreateTable(params)

	if err != nil {
		fmt.Println("Error:", err.Error())
		return
	}

	fmt.Println(res)
}
