package main

import (
	"github.com/gin-gonic/gin"
	"github.com/local/handlers"
)

func reqHandler() {
	router := gin.Default()

	// Insurance List
	router.GET("/insurance_list", handlers.InsuranceList)
	router.GET("/insurance_plans", handlers.InsurancePlans)

	// CRUD
	router.GET("/create_table", handlers.CreateTable)
	router.GET("/insert_item", handlers.InsertItem)
	router.GET("/update_item", handlers.UpdateItem)
	router.GET("/get_item", handlers.GetItem)
	router.GET("/delete_item", handlers.DeleteItem)

	router.Run(":12345")

}

func main() {
	reqHandler()
}
